import { useState, useEffect } from 'react'
import GoogleMapsApiLoader from 'google-maps-api-loader'

const useGoogleMap = apiKey => {
  const [googleMap, setGoogleMap] = useState(null)
  useEffect(() => {
    GoogleMapsApiLoader({ apiKey }).then(google => {
      setGoogleMap(google)
    })
  }, [])
  return googleMap
}

const useMap = ({ googleMap, mapContainerRef, initialConfig, latLng }) => {
  console.log(latLng)
  const [map, setMap] = useState(null)
  useEffect(() => {
    if (!googleMap || !mapContainerRef.current) {
      return
    }
    // 初始化
    const map = new googleMap.maps.Map(mapContainerRef.current, initialConfig)
    // marker
    const marker = new googleMap.maps.Marker({
      position: initialConfig.center,
      map: map
    })
    const InfoWindow = new googleMap.maps.InfoWindow({
      content: `<div id="content">
                    <button id="onBtn" class="btn btn-sm">
                      按鈕
                    </button>
                  </div>`
    })
    marker.addListener('click', () => {
      InfoWindow.open(map, marker)
    })
    setMap(map)
    map.setCenter(latLng)
    var markerLatLng = new googleMap.maps.LatLng(latLng.lat, latLng.lng)
    marker.setPosition(markerLatLng)
  }, [googleMap, mapContainerRef, latLng])
  return map
}

export { useGoogleMap, useMap }
