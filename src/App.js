import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Switch, Route, Link } from "react-router-dom";
import Home from "./views/Home/index";
import YouTubeAPI from "./views/YouTubeAPI/index";
import ZY135 from "./views/ZY135/index";
import ZY135VIEW from "./views/ZY135VIEW/index";
import Navbar from "./components/Navbar";
import Food from "./views/Food/index";
import Test from "./views/Test/index";
import TablePage from "./views/tablepage";
import DataTablePage from "./views/Component/datatablepage";
import P5jsDEMO from "./views/p5js/demo01";
import BookReview from './views/books/bookreview'
import Comic from './views/Comic'
import binarysearch from './views/algorithms/binarysearch'
class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/youtube" component={YouTubeAPI} />
          <Route path="/zy135" component={ZY135} />
          <Route path="/zy135view" component={ZY135VIEW} />
          <Route path="/food" component={Food} />
          <Route path="/test" component={Test} />
          <Route
            path="/components/tablepage/:currentPageLink"
            component={TablePage}
          />
          <Route path="/components/datatablepage" component={DataTablePage} />
          <Route path="/p5js/demo01" component={P5jsDEMO} />
          <Route path="/books/bookreview/:id" component={BookReview} />
          <Route path="/comic" component={Comic} />
          <Route path="/algorithms/binarysearch" component={binarysearch} />
          <Route component={NoMatch} />
        </Switch>
      </div>
    );
  }
}

function NoMatch({ location }) {
  return (
    <div>
      404
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

export default App;
