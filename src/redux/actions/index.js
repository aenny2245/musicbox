export const actionTypes = {
  CALL_API: "CALL:API",
  LOGIN: "LOGIN",
  LOGOUT: "LOGOUT"
};
