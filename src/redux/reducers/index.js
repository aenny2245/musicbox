import { combineReducers } from "redux";
import accountFn from "./accountFn";
import apiReducer from "./apiReducer";

export default combineReducers({
  accountFn,
  apiReducer
});
