import {actionTypes} from "../actions/index";

export default function(state = { login: false }, payload) {
  switch (payload.type) {
    case actionTypes.LOGIN:
      return {
        ...state,
        login: payload.account.password === "0207" && !state.login
      };
    case actionTypes.LOGOUT:
      // console.log(`${state.login}`);
      return {
        ...state,
        login: !state.login
      };
    default:
      return state;
  }
}
