import {actionTypes} from "../actions/index";

export default (state = false, action) => {
  switch (action.type) {
    case actionTypes.CALL_API:
      return true;
    default:
      return false;
  }
};
