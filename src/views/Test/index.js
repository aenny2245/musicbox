import React, { Component } from "react";
import ReactPlayer from "react-player";
const Test = () => {
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-sm-4">
          <h4>135 iframe</h4>
          <iframe
            src="https://ddppnew1.135-cdn.com/share/c92fRHtcD7CTONh4"
            width="100%"
            height="100%"
            frameBorder="0"
            scrolling="no"
            allowfullscreen="true"
            webkitallowfullscreen="true"
            mozallowfullscreen="true"
          />
        </div>
        <div className="col-sm-4">
          <h4>135 m3u8</h4>
          <ReactPlayer
            url="https://ddppnew1.135-cdn.com/20190519/3LaVZaKy/index.m3u8"
            controls
            width="100%"
            height="100%"
          />
        </div>
        <div className="col-sm-4">
          <h4>51 m3u8</h4>
          <ReactPlayer
            url="https://meigui.qqqq-kuyun.com/20190519/5697_48b3127a/index.m3u8"
            controls
            width="100%"
            height="100%"
          />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Test;
