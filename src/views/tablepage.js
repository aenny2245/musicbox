import React from "react";
import { withRouter } from "react-router-dom";
import TableBasic from "./Component/tablebasic";
import TableValidator from "./Component/tableValidator";
const TablePage = props => {
  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4 className="my-3">資料渲染</h4>
            <TableBasic {...props} />
          </div>
          <div className="col-12">
            <h4 className="my-3">資料修改、驗證</h4>
            <TableValidator {...props} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default withRouter(TablePage);
