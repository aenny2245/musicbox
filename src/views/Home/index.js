import React, { Component } from "react";
import { Link } from "react-router-dom";
const Home = () => {
  return (
    <div className="container mt-5">
      {/* <h3 className="mb-3">鳳凰是中國古代傳說中的百鳥之王</h3> */}
      <h4 className="text-deepgray">React</h4>
      <div className="row">
        <div className="col-sm-6">
          <h6>components</h6>
          <ul class="list-group">
            <li class="list-group-item">
              <Link to="/components/tablepage/1">表格</Link>
            </li>
            <li class="list-group-item">
              <Link to="/components/datatablepage">datatable</Link>
            </li>
          </ul>
        </div>

        <div className="col-sm-6">
          <h6>news</h6>
          <ul class="list-group">
            <li class="list-group-item">
              <a
                href="https://github.com/simonccarter/react-conf-videos"
                target="_blank"
              >
                react-conf-videos
              </a>
            </li>
          </ul>
        </div>
      </div>
      <h5 className="mt-3">WebGL</h5>
      <ul class="list-group">
        <li class="list-group-item">
          <Link to="/p5js/demo01">WebGL</Link>
        </li>
      </ul>
      <h5 className="mt-3">讀書會</h5>
      <ul class="list-group">
        <li class="list-group-item">
          <Link to="/books/bookreview/book001">思考致富聖經</Link>
        </li>
      </ul>
      <h5 className="mt-3">演算法</h5>
      <ul class="list-group">
        <li class="list-group-item">
          <Link to="/algorithms/binarysearch">Binary Search 二分演算法</Link>
        </li>
      </ul>
    </div>
  );
};

export default Home;
