import React, { Component } from "react";
import P5Wrapper from "react-p5-wrapper";
import sketch1 from "../../components/p5js/sktech1";
import sketch2 from "../../components/p5js/sktech2";
import sketch3 from "../../components/p5js/sktech3";
import sketch4 from "../../components/p5js/sktech4";

const P5jsDEMO = () => {
  return (
    <div className="container-fluid">
      <div className="row justify-content-center">
        {/* 形狀 */}
        <h4 className="py-3">形狀</h4>
        <P5Wrapper sketch={sketch1} />
        {/* 打光 */}
        <h4 className="py-3">打光</h4>
        <P5Wrapper sketch={sketch2} />
        <h4 className="py-3">載入圖片(texture)</h4>
        <P5Wrapper sketch={sketch3} />
        <h4 className="py-3">鏡頭</h4>
        <P5Wrapper sketch={sketch4} />
      </div>
    </div>
  );
};

export default P5jsDEMO;
