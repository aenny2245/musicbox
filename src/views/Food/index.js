import React, { useState } from 'react'
import Gmap from '../../components/Gmap'
import './index.module.scss'
import datas from '../../datas/TopFood.json'
const Food = () => {
  const [address, useAddress] = useState()
  const [latLng, setLatLng] = useState({})
  const searchMap = (e, data) => {
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${`台中市${data['地址']}`}&key=AIzaSyCmeCDLj1uvr97KxjUiaade1zfeya02Uog`
    fetch(url)
      .then(res => {
        return res.json()
      })
      .then(json => {
        setLatLng(json.results[0].geometry.location)
      })
  }
  return (
    <div className="full-container">
      <div className="left">
        <Gmap address={address} latLng={latLng} />
      </div>

      <div className="right">
        <h4>
          臺中市百大名攤名產 <span>2018-05-04</span>
        </h4>

        <dl>
          <div className="dl-head">
            <div className="dl-tr">
              <dt>編號</dt>
              <dt>區域</dt>
              <dt>地址</dt>
              <dt>攤名</dt>
              <dt>電話</dt>
            </div>
          </div>
          <div className="dl-body">
            {datas.ROOT.RECORD.map((data, idx) => {
              return (
                <div key={idx} className="dl-tr" onClick={e => searchMap(e, data)}>
                  <dd>{data.編號}</dd>
                  <dd>{data.區域}</dd>
                  <dd>{data.地址}</dd>
                  <dd>{data.攤名}</dd>
                  <dd>{data.電話}</dd>
                </div>
              )
            })}
          </div>
        </dl>
      </div>
    </div>
  )
}

export default Food
