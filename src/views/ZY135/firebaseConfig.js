// firebase config
var firebase = window.firebase;
export const getFirebase = async cb => {
  var config = {
    apiKey: "AIzaSyD8F-WzwqYBP0wK4PjfFCBas7DP4_h72mY",
    authDomain: "tvbox-7ebb7.firebaseapp.com",
    databaseURL: "https://tvbox-7ebb7.firebaseio.com",
    storageBucket: "tvbox-7ebb7.appspot.com",
    projectId: "tvbox-7ebb7"
  };
  if (!firebase.apps.length) {
    var app = firebase.initializeApp(config);
  } else {
    var app = firebase;
  }
  let dbRef = app.database().ref();
  dbRef.on("value", snap => {
    var data = snap.val();
    cb(data);
  });
};
