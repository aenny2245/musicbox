import React, { useState, useEffect, useRef } from "react";
import {Link} from 'react-router-dom'
import SpringCard from "../../components/SpingCard";
import { Player } from "video-react";
import HLSSource from "../../components/HLSSource";
import iconSetting from "../../static/img/icon-setting.svg";
const CardCard = ({ firebaseData, data, idx, choiseVideo, login,_PlayYun,_Play }) => {
  var config = {
    apiKey: "AIzaSyD8F-WzwqYBP0wK4PjfFCBas7DP4_h72mY",
    authDomain: "tvbox-7ebb7.firebaseapp.com",
    databaseURL: "https://tvbox-7ebb7.firebaseio.com",
    storageBucket: "tvbox-7ebb7.appspot.com",
    projectId: "tvbox-7ebb7"
    // storageBucket: "",
    // messagingSenderId: "141415165779"
  };
  if (!firebase.apps.length) {
    var app = firebase.initializeApp(config);
  } else {
    var app = firebase;
  }
  // ----^^^^  firebase config

  let dbRef = app.database().ref();

  const [input, setInput] = useState({
    inputURL: "",
    m3u8URL: "",
    cardPassword: ""
  });
  const [correctPassowrd, setCorrectPassowrd] = useState(false);
  const [cardSettingFn, setCardSettingFn] = useState(false);
  useEffect(() => {
    if (+input.cardPassword === 777) {
      setCorrectPassowrd(true);
    } else {
      setCorrectPassowrd(false);
    }
  }, [input.cardPassword]);
  const handleChange = e => {
    const { name, value } = e.target;
    setInput({ ...input, [name]: value });
  };
  const videoCardSetting = e => {
    setCardSettingFn(!cardSettingFn);
    e.stopPropagation();
    e.preventDefault();
  };
  return (
    <Link
      key={idx}
      className={`card mr-3 text-white bg-dark`}
      style={{ width: "18rem", minWidth: "100px" }}
      to={{
        pathname: "/zy135",
        search: `?video_title=${firebaseData[data]["info"].originTitle}`
      }}
      onClick={e => choiseVideo(e, data)}
    >
      <SpringCard
        class="card-img-top"
        imgUrl={`${firebaseData[data]["info"].img}`}
      />
      {login ? (
        <button className="btn-setting" onClick={videoCardSetting}>
          <img src={iconSetting} />
        </button>
      ) : null}
      <div className="card-body">
        <p className="card-text">{firebaseData[data]["info"].originTitle}</p>
        {JSON.parse(localStorage.getItem("video_record")).map((item, i) => {
          if (item.video_title === firebaseData[data]["info"].originTitle) {
            if (/.m3u8/.test(item.data.url)) {
              return (
                <Link
                  className="btn btn-sm btn-info"
                  title="record"
                  to={{
                    pathname: "/zy135",
                    search: `?video_title=${item.video_title}&video_num=${item.data.num}`
                  }}
                  onClick={e =>
                    _Play(e, {
                      data: item.data,
                      video_title: item.video_title
                    })
                  }
                >
                  繼續播放{item.data.num}
                </Link>
              );
            } else {
              return (
                <Link
                  className="btn btn-sm btn-info"
                  title="record"
                  to={{
                    pathname: "/zy135",
                    search: `?video_title=${item.video_title}&video_num=${item.data.num}`
                  }}
                  onClick={e =>
                    _PlayYun(e, {
                      data: item.data,
                      video_title: item.video_title
                    })
                  }
                >
                  繼續播放{item.data.num}
                </Link>
              );
            }
          }
        })}
      </div>
      <div className={`card-foot ${cardSettingFn && "toggle"}`}>
        {!correctPassowrd && (
          <input
            type="text"
            name="cardPassword"
            className="form-control"
            placeholder="請輸入密碼"
            onChange={handleChange}
          />
        )}
        {correctPassowrd && (
          <button
            className="btn btn-sm btn-danger"
            onClick={() => {
              dbRef.child(data).remove();
              setCorrectPassowrd(false);
              setCardSettingFn(false);
            }}
          >
            刪除
          </button>
        )}
      </div>
    </Link>
  );
};

export default CardCard;
