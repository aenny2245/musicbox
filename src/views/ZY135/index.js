import React, { useState, useEffect, useRef } from "react";
import ReactPlayer from "react-player";
import { Link } from "react-router-dom";
import "./styles.module.scss";
import Footer from "../../components/layout/footer";
import { getFirebase } from "./firebaseConfig";
import "../../static/video-react.css";

import CardCard from "./Card";

import { connect } from "react-redux";
const ZY135 = ({ location, login }) => {
  const [isPlayer, setIsPlayer] = useState(""); //播放器
  const [updateStatus, setUpdateStatus] = useState("initial"); //影片來源
  const [firebaseData, setFirebaseData] = useState([]); //firebase 資料

  const [input, setInput] = useState({
    inputURL: "",
    m3u8URL: "",
    cardPassword: ""
  });
  const [videoInfo, setVideoInfo] = useState({ title: "", num: "" }); //影片資訊
  const [url, setUrl] = useState({ url: "" });
  const [zyurl, setZYURL] = useState({ zyurl: "" });
  const [isVideoType, setIsVideoType] = useState(false);

  const [stashData, setStashData] = useState(
    JSON.parse(localStorage.getItem("video_record")) || []
  ); //存取播放紀錄

  useEffect(() => {
    getFirebase(res => {
      setFirebaseData(res);
    });
  }, []);

  useEffect(() => {
    localStorage.setItem("video_record", JSON.stringify(stashData));
  });
  // 監聽 url 變更 callback function
  useEffect(() => {
    _loadParameters();
  }, [url, zyurl]);

  useEffect(() => {
    setIsVideoType(false)
  }, [isVideoType]);
  const handleChange = e => {
    const { name, value } = e.target;
    setInput({ ...input, [name]: value });
  };
  const localStorageStash = datas => {
    // 存取播放紀錄
    if (stashData.length === 0) {
      const arr = [];
      arr.push({
        video_title: datas.video_title,
        data: datas.data
      });
      setStashData(arr);
    } else {
      // 只更新點擊的影片資訊
      const filterData = stashData.filter(
        (item, i) => item.video_title === video_title
      );
      stashData.map(item => {
        if (item.video_title === video_title) {
          filterData[0]["data"] = datas.data;
          setStashData(stashData);
        }
      });

      // 如果都沒有影片，才會新增
      const everyTitle = stashData.every(
        (item, i) => video_title !== item.video_title
      );
      if (everyTitle) {
        stashData.push({
          video_title: datas.video_title,
          data: datas.data
        });
      }
    }
  };
  const _Play = (e, datas) => {
    let video_title = new URLSearchParams(location.search).get("video_title");
    if (e.target.title !== "record") {
      localStorageStash(datas);
    }
    setIsPlayer("m3u8player");
    setUrl({ ...url, url: datas.data.url });
  };
  const _PlayZY = (e, num) => {
    let video_title = new URLSearchParams(location.search).get("video_title");
    for (let i in firebaseData[video_title]["data"]) {
      if (num === firebaseData[video_title]["data"][i].num) {
        setIsPlayer("zyplayer");
        setZYURL({
          ...url,
          url: firebaseData[video_title]["data"][i].zyURL.replace(/http:/, "")
        });
      }
    }
  };
  const _PlayYunM3U8 = (e, datas) => {
    const { num } = datas;
    let video_title = new URLSearchParams(location.search).get("video_title");
    for (let i in firebaseData[video_title]["data"]) {
      if (num === firebaseData[video_title]["data"][i].num) {
        if (e.target.title !== "record") {
          localStorageStash({ video_title: video_title, data: datas });
        }
        setIsPlayer("m3u8player");
        setUrl({ ...url, url: datas.kkm3u8Url });
      }
    }
  };
  const _PlayYun = (e, datas) => {
    // console.log(datas);
    const { num } = datas;
    let video_title = new URLSearchParams(location.search).get("video_title");
    for (let i in firebaseData[video_title]["data"]) {
      if (num === firebaseData[video_title]["data"][i].num) {
        if (e.target.title !== "record") {
          localStorageStash({ video_title: video_title, data: datas });
        }
        setIsPlayer("zyplayer");
        setZYURL({
          ...url,
          url: firebaseData[video_title]["data"][i].kkyunUrl.replace(
            /http:/,
            ""
          )
        });
      }
    }
  };
  const _m3u8Play = e => {
    setUrl({ url: input.m3u8URL });
    if (/.m3u8/.test(input.m3u8URL)) {
      setIsPlayer("m3u8player");
    } else {
      setIsPlayer("zyplayer");
    }
  };
  const _loadParameters = () => {
    let video_title = new URLSearchParams(location.search).get("video_title");
    let video_num = new URLSearchParams(location.search).get("video_num");
    setVideoInfo({ ...videoInfo, title: video_title, num: video_num });
  };
  const addData = e => {
    // post
    var url = "https://lets-api.appspot.com/item/apiOne";
    var data = { title: input.inputTitle, url: input.inputURL };
    console.log(data)

    fetch(url, {
      method: "POST", // or 'PUT'
      body: JSON.stringify(data), // data can  be `string` or {object}!
      headers: new Headers({
        "Content-Type": "application/json"
      })
    })
      .then(res => res.json())
      .catch(error => console.error("Error:", error))
      .then(response => console.log("Success:", response));
  };
  const choiseVideo = (e, val) => {
    const originURL = firebaseData[val]["info"].originURL;
    const url = new URL(originURL);
    switch (url.host) {
      case "www.kuyunzy1.com":
        localStorage.setItem("videoType", "kkyun");
        setIsVideoType(true)
        break;
      case "135zy0.com":
        localStorage.setItem("videoType", "sourceM3U8");
        setIsVideoType(true)
        break;
    }
  };
  const updateData = (e, videoTitle) => {
    if (firebaseData) {
      setUpdateStatus("updating");
      const originURL = firebaseData[videoTitle].info.originURL;
      // post
      var url = "https://lets-api.appspot.com/item/apiOne";
      var data = { title: videoTitle, url: originURL };
      fetch(url, {
        method: "POST", // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: new Headers({
          "Content-Type": "application/json"
        })
      })
        .then(res => res.json())
        .catch(error => console.error("Error:", error))
        .then(response => {
          setUpdateStatus("initial");
          console.log("Success:", response);
        });
    }
  };

  const changeSource = (e, source) => {
    localStorage.setItem("videoType", source);
    setIsVideoType(true);
  };
  const clearLocalStorage = () => {
    localStorage.clear();
    window.location.reload();
  };
  const renderInfobox = source => {
    let video_title = new URLSearchParams(location.search).get("video_title");
    let video_num = new URLSearchParams(location.search).get("video_num");
    let ul = [];
    let li = [];
    if (firebaseData[video_title]) {
      const datas = firebaseData[video_title]["data"];
      datas &&
        Object.values(datas).map((data, i) => {
          switch (source) {
            case "sourceM3U8":
              li.push(
                <li>
                  <Link
                    className={`btn btn-sm mr-2 ${
                      data.num === video_num ? "active" : ""
                    }`}
                    to={{
                      pathname: "/zy135",
                      search: `?video_title=${video_title}&video_num=${data.num}`
                    }}
                    onClick={e => _Play(e, { data, video_title })}
                  >
                    {data.num}
                    <span style={{ float: "right" }}>
                      {" "}
                      {Object.keys(data).includes("date") ? data.date : null}
                    </span>
                  </Link>
                </li>
              );
              break;
            case "sourceZY":
              li.push(
                <li>
                  <Link
                    className={`btn btn-sm ${
                      data.num === video_num ? "active" : ""
                    }`}
                    to={{
                      pathname: "/zy135",
                      search: `?video_title=${video_title}&video_num=${data.num}`
                    }}
                    onClick={e => _PlayZY(e, `${data.num}`)}
                  >
                    {data.num}
                    <span style={{ float: "right" }}>
                      {" "}
                      {Object.keys(data).includes("date") ? data.date : null}
                    </span>
                  </Link>
                </li>
              );
              break;
            case "kkyun":
              li.push(
                <li>
                  <Link
                    className={`btn btn-sm mr-2 ${
                      data.num === video_num ? "active" : ""
                    }`}
                    to={{
                      pathname: "/zy135",
                      search: `?video_title=${video_title}&video_num=${data.num}`
                    }}
                    onClick={e => _PlayYun(e, data)}
                  >
                    {data.num}
                    <span style={{ float: "right" }}>
                      {Object.keys(data).includes("date") ? data.date : null}
                    </span>
                  </Link>
                </li>
              );
              break;
            case "kkm3u8":
              li.push(
                <li>
                  <Link
                    className={`btn btn-sm mr-2 ${
                      data.num === video_num ? "active" : ""
                    }`}
                    to={{
                      pathname: "/zy135",
                      search: `?video_title=${video_title}&video_num=${data.num}`
                    }}
                    onClick={e => _PlayYunM3U8(e, data)}
                  >
                    {data.num}
                    <span style={{ float: "right" }}>
                      {Object.keys(data).includes("date") ? data.date : null}
                    </span>
                  </Link>
                </li>
              );
          }
        });
    }
    ul.push(<ul className="list-group react-player-numlist">{li}</ul>);
    return ul;
  };

  let video_title = new URLSearchParams(location.search).get("video_title");
  const isisSource = localStorage.getItem("videoType");
  return (
    <React.Fragment>
      <div
        className="nav-videolist d-flex pt-3 px-3"
        style={{ overflowX: "auto" }}
      >
        {/* Object.keys(firebaseData) */}
        {Object.keys(firebaseData).length === 0 ? (
          <span>讀取影片中 ...</span>
        ) : (
          Object.keys(firebaseData).map((val, idx) => {
            // console.log(firebaseData[val]["info"].img)
            return (
              <CardCard
                firebaseData={firebaseData}
                data={val}
                idx={idx}
                choiseVideo={choiseVideo}
                login={login}
                _Play={_Play}
                _PlayYun={_PlayYun}
              />
            );
          })
        )}
      </div>
      <div className="container">
        <div className="py-1">
          <h4>
            <span className="text-mainColor mr-3">{videoInfo.title}</span>
            <span className="text-subColor">{videoInfo.num}</span>
          </h4>
        </div>
        <div className="player-layout good-shadow">
          <div className="row no-gutters">
            <div className="col-md-9 player-layout-l">
              {isPlayer === "m3u8player" ? (
                <div className="player-wrapper">
                  <ReactPlayer
                    className="react-player"
                    url={url.url.replace(/(http|https):/, "")}
                    controls
                    playing
                    width="100%"
                    height="100%"
                    pip={true}
                  />
                </div>
              ) : isPlayer == "zyplayer" ? (
                <div className="player-wrapper">
                  {/* {console.log(zyurl.url)} */}
                  <iframe
                    src={zyurl.url}
                    width="100%"
                    height="100%"
                    frameBorder="0"
                    scrolling="no"
                    allowfullscreen="true"
                    webkitallowfullscreen="true"
                    mozallowfullscreen="true"
                    style={{ position: "absolute", left: "0", top: "0" }}
                  />
                </div>
              ) : (
                <div className="player-wrapper" />
              )}
            </div>

            <div className="col-md-3 player-layout-r p-3">
              <div className="d-flex flex-nowrap align-items-center">
                <div className="mr-auto">
                  <div>
                    {isisSource === "kkyun" || isisSource === "kkm3u8" ? (
                      <div>
                        <button
                          className={`btn btn-sm btn-link text-white ${isisSource ===
                            "kkyun" && "good-gradient-primary"}`}
                          onClick={e => changeSource(e, "kkyun")}
                        >
                          kkyun
                        </button>
                        <button
                          className={`btn btn-sm btn-link text-white ${isisSource ===
                            "kkm3u8" && "good-gradient-primary"}`}
                          onClick={e => changeSource(e, "kkm3u8")}
                        >
                          kkm3u8
                        </button>
                      </div>
                    ) : (
                      <div>
                        <button
                          className={`btn btn-sm btn-link text-white ${isisSource ===
                            "sourceM3U8" && "good-gradient-primary"}`}
                          onClick={e => changeSource(e, "sourceM3U8")}
                        >
                          片源一
                        </button>
                        <button
                          className={`btn btn-sm btn-link text-white ${isisSource ===
                            "sourceZY" && "good-gradient-primary"}`}
                          onClick={e => changeSource(e, "sourceZY")}
                        >
                          片源二
                        </button>
                      </div>
                    )}
                  </div>
                </div>
                <div className="ml-auto">
                  {updateStatus === "initial" ? (
                    <button
                      className="btn btn-sm btn-link good-gradient-primary text-white mb-2"
                      onClick={e => updateData(e, video_title)}
                    >
                      更新
                    </button>
                  ) : updateStatus === "updating" ? (
                    <button
                      className="btn btn-sm btn-link good-gradient-primary text-white mb-2"
                      onClick={e => updateData(e, video_title)}
                      disabled
                    >
                      更新中 ...
                    </button>
                  ) : null}
                </div>
              </div>
              <div className="renderInfoBox">
                {isisSource === "kkyun" ? renderInfobox("kkyun") : null}
                {isisSource === "kkm3u8" ? renderInfobox("kkm3u8") : null}
                {isisSource === "sourceZY" ? renderInfobox("sourceZY") : null}
                {isisSource === "sourceM3U8"
                  ? renderInfobox("sourceM3U8")
                  : null}
              </div>
            </div>
          </div>
        </div>

        <div className="my-5">
          {login ? (
            <div className="row mt-3 border border-primary mx-1 px-4">
              <div className="form-group">
                <label className="col-form-label col-auto mr-1">來源</label>
                <div className="col-auto w-100">
                  <a
                    className="btn btn-primary text-white btn-sm mr-1 mb-1"
                    href="http://135zy.cc/"
                    target="_blank"
                  >
                    135zy
                  </a>

                  <a
                    className="btn btn-primary text-white btn-sm mr-1 mb-1"
                    href="http://www.kuyunzy1.com/"
                    target="_blank"
                  >
                    kuyunzy
                  </a>
                  <br />
                  <a
                    className="btn btn-primary text-white btn-sm mr-1 mb-1"
                    href="https://51kdy.org/"
                    target="_blank"
                  >
                    51kdy(尚未爬)
                  </a>
                </div>
              </div>
              <div className="form-group">
                <label className="col-form-label col-auto mr-1">影片網址</label>
                <div className="col-auto w-100">
                  <input
                    type="text"
                    className="form-control mb-1"
                    name="inputURL"
                    value={input.inputURL}
                    onChange={handleChange}
                    placeholder="輸入網址"
                  />
                  <input
                    type="text"
                    className="form-control mb-1"
                    name="inputTitle"
                    value={input.inputTitle}
                    onChange={handleChange}
                    placeholder="輸入標題"
                  />
                  <button onClick={e => addData(e)}>新增</button>
                </div>
              </div>
              <div className="form-group">
                <label className="col-form-label col-auto mr-1">
                  m3u8 網址播放
                </label>
                <div className="col-auto w-100">
                  <input
                    type="text"
                    className="form-control mb-1"
                    name="m3u8URL"
                    value={input.m3u8URL}
                    onChange={handleChange}
                    placeholder="輸入網址"
                  />
                  <button onClick={e => _m3u8Play(e)}>立馬播</button>
                </div>
              </div>
              <div className="form-group">
                <label className="col-form-label col-auto mr-1">小工具</label>
                <div className="col-auto w-100">
                  <button onClick={clearLocalStorage}>清除紀錄</button>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>

      <Footer />
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    login: state.accountFn.login
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: () => dispatch({ type: "LOGIN" }),
    onLogout: () => dispatch({ type: "LOGIN" })
  };
};

const connectZY135 = connect(
  mapStateToProps,
  mapDispatchToProps
)(ZY135);

export default connectZY135;
