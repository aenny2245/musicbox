import React, { useEffect, useState } from "react";
import Table from "../../components/Table";
import tableDatas from "../../datas/demo.json";
const TableBasic = (props) => {
  const tableColumn = [
    {
      prop: "idx",
      name: "作者",
      render: function(datas, idx) {
        return idx + 1;
      }
    },
    {
      prop: "first_name",
      name: "作者",
      render: function(datas) {
        return datas.user.first_name;
      }
    },
    {
      prop: "regular",
      name: "圖片",
      render: function(datas) {
        return (
          <img
            src={datas.urls.regular}
            className="img-fluid"
            style={{ maxWidth: "200px" }}
          />
        );
      }
    },

    {
      prop: "description",
      name: "描述"
    },
    {
      prop: "description",
      name: "下載",
      render: function(datas) {
        return (
          <a href={datas.urls.full} target="_blank">
            1080p
          </a>
        );
      }
    }
  ];

  return (
    <Table
      summary="unsplash"
      column={tableColumn}
      datas={tableDatas.results}
      pagination={[5, 10, 20]}
      paginationInit="5"
      className="table"
      theme="primary"
      currentPageLink={props.match.params.currentPageLink}
    />
  );
};

export default TableBasic;
