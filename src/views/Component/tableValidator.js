import React, { useEffect, useState } from "react";
import Table from "../../components/Table";
import tableDatas from "../../datas/demo.json";
import Validator from "async-validator";
const TableValidator = props => {
  const [newData, setNewData] = useState([]);
  const [form, setForm] = useState({});
  const [editIndex, setEditIndex] = useState(0);
  const [formField, setFormField] = useState({});
  useEffect(() => {
    const data = tableDatas.results.map((data, i) => {
      data["toggle"] = false;
      return data;
    });
    setNewData(data);
  }, tableDatas.results);
  useEffect(() => {
    const formRules = formFieldFn(editIndex);
    const validator = new Validator(formRules);
    validator.validate(form, (errors, fields) => {
      if (errors) {
        setFormField(fields);
        return false;
      }
      console.log("success");
    });
  }, [
    form[`user${editIndex}`],
    form[`imgURL${editIndex}`],
    form[`description${editIndex}`],
    form[`link${editIndex}`]
  ]);

  const tableColumnValidator = [
    {
      prop: "idx",
      name: "#",
      render: function(datas, index) {
        return index + 1;
      }
    },
    {
      prop: "first_name",
      name: "作者",
      render: function(datas, index) {
        if (datas.toggle) {
          const isTrue = Object.keys(formField).includes(`user${index}`);
          return (
            <React.Fragment>
              <input
                name={`user${index}`}
                value={form[`user${index}`]}
                type="text"
                className="form-control"
                placeholder="請輸入作者"
                onChange={handleChange}
              />
              <span className="text-danger">
                {isTrue && formField[`user${index}`][0].message}
              </span>
            </React.Fragment>
          );
        } else {
          return datas.user.first_name;
        }
      }
    },
    {
      prop: "regular",
      name: "圖片",
      render: function(datas, index) {
        if (datas.toggle) {
          const isTrue = Object.keys(formField).includes(`imgURL${index}`);
          return (
            <React.Fragment>
              <input
                name={`imgURL${index}`}
                value={form[`imgURL${index}`]}
                type="text"
                className="form-control"
                placeholder="請輸入圖片網址"
                onChange={handleChange}
              />
              <span className="text-danger">
                {isTrue && formField[`imgURL${index}`][0].message}
              </span>
            </React.Fragment>
          );
        } else {
          return (
            <img
              src={datas.urls.regular}
              className="img-fluid"
              style={{ maxWidth: "200px" }}
            />
          );
        }
      }
    },

    {
      prop: "description",
      name: "描述",
      render: function(datas, index) {
        if (datas.toggle) {
          const isTrue = Object.keys(formField).includes(`description${index}`);
          return (
            <React.Fragment>
              <input
                name={`description${index}`}
                value={form[`description${index}`]}
                type="text"
                className="form-control"
                placeholder="請輸入描述"
                onChange={handleChange}
              />
              <span className="text-danger">
                {isTrue && formField[`description${index}`][0].message}
              </span>
            </React.Fragment>
          );
        } else {
          return <span>{datas.description}</span>;
        }
      }
    },
    {
      prop: "",
      name: "下載",
      render: function(datas, index) {
        if (datas.toggle) {
          const isTrue = Object.keys(formField).includes(`link${index}`);
          return (
            <React.Fragment>
              <input
                name={`link${index}`}
                value={form[`link${index}`]}
                type="text"
                className="form-control"
                placeholder="請輸入圖片連結"
                onChange={handleChange}
              />
              <span className="text-danger">
                {isTrue && formField[`link${index}`][0].message}
              </span>
            </React.Fragment>
          );
        } else {
          return (
            <a href={datas.urls.full} target="_blank">
              1080p
            </a>
          );
        }
      }
    },
    {
      prop: "",
      name: "操作",
      render: function(datas, index) {
        if (datas.toggle) {
          return (
            <React.Fragment>
              <button
                className="btn btn-sm btn-secondary"
                onClick={e => noModify(e, index)}
              >
                不修改
              </button>
              <button
                className="btn btn-sm btn-secondary"
                onClick={e => fakeData(e, index)}
              >
                假資料
              </button>
              <button
                className="btn btn-sm btn-success"
                onClick={e => submitModify(e, index)}
              >
                確定
              </button>
            </React.Fragment>
          );
        } else {
          return (
            <React.Fragment>
              <button
                className="btn btn-sm btn-primary"
                onClick={e => editTable(e, index)}
              >
                編輯
              </button>
            </React.Fragment>
          );
        }
      }
    }
  ];

  const formFieldFn = index => {
    const formRules = {};
    formRules[`user${index}`] = [
      { type: "string", required: true, message: "請輸入帳號" }
    ];
    formRules[`imgURL${index}`] = [
      { type: "string", required: true, message: "請輸入圖片網址" }
    ];
    formRules[`description${index}`] = [
      { type: "string", required: true, message: "請輸入描述" }
    ];
    formRules[`link${index}`] = [
      { type: "string", required: true, message: "請輸入連結" }
    ];
    return formRules;
  };

  const handleChange = e => {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
    setFormField({});
  };
  const editTable = (e, index) => {
    newData[index].toggle = true;
    setNewData(newData);
    setEditIndex(index);
  };
  const noModify = (e, index) => {
    newData[index].toggle = false;
    setNewData(newData);
  };

  const fakeData = (e, index) => {
    const f = {}
    f[`user${index}`] = "Disney";
    f[`imgURL${index}`] = "https://cw1.tw/CC/images/article/J1413450885956.jpg";
    f[`description${index}`] = "冰雪奇緣";
    f[`link${index}`] = "https://cw1.tw/CC/images/article/J1413450885956.jpg";
    setForm(f);
  };

  const submitModify = (e, index) => {
    const formRules = formFieldFn(index);
    const validator = new Validator(formRules);
    validator.validate(form, (errors, fields) => {
      if (errors) {
        setFormField(fields);
        return false;
      }
      console.log("success");
      newData[index].toggle = false;
      newData[index].description = form[`description${index}`];
      newData[index].user.first_name = form[`user${index}`];
      newData[index].urls.regular = form[`imgURL${index}`];
      newData[index].urls.full = form[`link${index}`];
      setNewData(newData);
    });
  };

  return (
    <Table
      summary="unsplash"
      column={tableColumnValidator}
      datas={newData}
      pagination={[5, 10, 20]}
      paginationInit="5"
      className="table"
      theme="primary"
      currentPageLink={props.match.params.currentPageLink}
    />
  );
};

export default TableValidator;
