import React, { useState, useEffect, useRef, createRef } from "react";
import Validator from "async-validator";

const Tr = props => {
  const [account, setAccount] = useState({ account: "", password: "" });
  const [accountField, setAccountField] = useState({});
  const [toggle, setToggle] = useState(false);

  const handleChange = e => {
    const { name, value } = e.target;
    setAccount({ ...account, [name]: value });
    setAccountField({}); // reset
  };
  const updateTable = (e, data) => {
    setToggle(!toggle);
    e.preventDefault();
  };
  const okTable = (e, data) => {
    const validator = new Validator(props.formRules);
    validator.validate(account, (errors, fields) => {
      if (errors) {
        setAccountField(fields);
        return false;
      }
      console.log("success");
      setAccountField({}); // reset

      // 1. 找編輯的資料位置
      const index = props.formData.findIndex(
        (v, i) => v.account === data.account
      );
      // 2. 加入更新後的資料
      const newFormData = props.formData;
      newFormData[index] = account;
      props.setFormData([...newFormData]);
      setToggle(!toggle);
    });
    e.preventDefault();
  };
  const removeAccount = (e, data) => {
    // 1. 目前點擊的帳號 = data
    // 2. 找帳號在資料的 index
    // 3. 運用 Array.splice 移除資料
    const index = props.formData.findIndex(
      (v, i) => v.account === data.account
    );
    props.formData.splice(index, 1);
    props.setFormData([...props.formData]);

    e.preventDefault();
  };
  return (
    <tr>
      <td>
        {toggle ? (
          <React.Fragment>
            <input
              type="text"
              placeholder="請輸入帳號"
              name="account"
              onChange={handleChange}
              value={account.account}
              className="form-control"
            />
            <div name="error" className="text-danger">
              {Object.keys(accountField).length &&
              Object.keys(accountField).includes("account")
                ? accountField.account[0].message
                : null}
            </div>
          </React.Fragment>
        ) : (
          props.data.account
        )}
      </td>
      <td>
        {toggle ? (
          <React.Fragment>
            <input
              type="text"
              placeholder="請輸入密碼"
              name="password"
              onChange={e => handleChange(e)}
              value={account.password}
              className="form-control"
            />
            <div name="error" className="text-danger">
              {Object.keys(accountField).length &&
              Object.keys(accountField).includes("password")
                ? accountField.password[0].message
                : null}
            </div>
          </React.Fragment>
        ) : (
          props.data.password
        )}
      </td>
      <td>
        {toggle ? (
          <React.Fragment>
            <button
              className="btn btn-sm btn-info mr-1"
              onClick={e => okTable(e, props.data)}
            >
              確定
            </button>
            <button
              onClick={e => {
                setToggle(!toggle);
                setAccountField({});
              }} // reset}
              className="btn btn-sm btn-danger"
            >
              不修改
            </button>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <button
              className="btn btn-sm btn-info mr-1"
              onClick={e => updateTable(e, props.data)}
            >
              編輯
            </button>
            <button
              onClick={e => removeAccount(e, props.data)}
              className="btn btn-sm btn-danger"
            >
              刪除
            </button>
          </React.Fragment>
        )}
      </td>
    </tr>
  );
};

const DataTablePage = props => {
  const [account, setAccount] = useState({ account: "", password: "" });
  const [accountField, setAccountField] = useState({});
  const [formData, setFormData] = useState([]);
  const formRules = {
    account: [
      { type: "string", required: true, message: "請輸入帳號" },
      {
        validator(rule, value, callback, source, options) {
          if (formData.some((v, i) => v.account === value)) {
            callback(new Error("此帳號重複囉~再發揮一點想像力吧！"));
          } else {
            callback();
          }
        }
      }
    ],
    password: [{ type: "string", required: true, message: "請輸入密碼" }]
  };

  const addAccount = () => {
    const validator = new Validator(formRules);
    validator.validate(account, (errors, fields) => {
      if (errors) {
        setAccountField(fields);
        return false;
      }
      console.log("success");
      setAccountField({}); // reset
      setAccount({ account: "", password: "" });
      setFormData([...formData, account]);
    });
  };
  const handleChange = e => {
    const { name, value } = e.target;
    setAccount({ ...account, [name]: value });
    setAccountField({}); // reset
  };
  const submitFormData = e => {
    console.log(formData);
  };
  return (
    <div className="container">
      <h4 className="mt-3 mb-5">帶驗證功能的 data-table</h4>
      <div className="row mt-3 px-3">
        <div className="form-group mr-2">
          <input
            type="text"
            placeholder="請輸入帳號"
            name="account"
            onChange={handleChange}
            value={account.account}
            className="form-control"
          />
          <div name="error" className="text-danger">
            {Object.keys(accountField).length &&
            Object.keys(accountField).includes("account")
              ? accountField.account[0].message
              : null}
          </div>
        </div>
        <div className="form-group mr-2">
          <input
            type="text"
            placeholder="請輸入密碼"
            name="password"
            onChange={e => handleChange(e)}
            value={account.password}
            className="form-control"
          />
          <div name="error" className="text-danger">
            {Object.keys(accountField).length &&
            Object.keys(accountField).includes("password")
              ? accountField.password[0].message
              : null}
          </div>
        </div>
        <div className="form-group">
          <button onClick={addAccount} className="btn btn-primary">
            新增
          </button>
        </div>
      </div>
      <table className="table">
        <thead className="bg-primary">
          <tr>
            <th>帳號</th>
            <th>密碼</th>
            <th>操作</th>
          </tr>
        </thead>
        <tbody>
          {formData &&
            formData.map((data, i) => {
              return (
                <Tr
                  key={i}
                  data={data}
                  formRules={formRules}
                  formData={formData}
                  setFormData={setFormData}
                />
              );
            })}
        </tbody>
      </table>
      <div className="text-center">
        {formData.length === 0 ? null : (
          <button onClick={submitFormData} className="btn btn-success">
            儲存
          </button>
        )}
      </div>

      {formData.length === 0 ? null : (
        <React.Fragment>
          <hr />
          <h4>目前的資料結構</h4>
          <pre>
            <code>{JSON.stringify(formData, null, 2)}</code>
          </pre>
        </React.Fragment>
      )}
    </div>
  );
};

export default DataTablePage;
