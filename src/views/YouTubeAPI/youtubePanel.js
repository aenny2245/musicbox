import React, { Component } from "react";
class YoutubePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      whichPlay: ""
    };
  }
  componentWillReceiveProps(nextProps) {
    //  console.log(nextProps)
    if (nextProps.nowPlay != this.props.nowPlay) {
      // console.log(`onFunction: ${nextProps.whichPlaybtn}`);
    }
  }
  render() {
    const { nowPlay, isFavorite, addToFavorite, scrollDirect } = this.props;
   
    return (
      <div className={`Footer-Block ${scrollDirect}`}>
        <div className="d-flex align-items-center">
          <div className="flex-shrink-0">
            {/* <select className="form-control form-control-sm mb-2">
              <option>全部重複播放</option>
              <option>單曲重複播放</option>
              <option>不重複播放</option>
            </select> */}
            <button
              className="btn btn-link"
              onClick={e => this.props.onPrev(e)}
            >
              <i class="fas fa-backward" />
            </button>
            {this.props.whichPlay === "playall" && (
              <button
                className="btn btn-play"
                onClick={e => this.props.onPlayVideoAll(e)}
              >
                <i className="fas fa-play" />
              </button>
            )}
            {this.props.whichPlay === "pause" && (
              <button
                className="btn btn-play"
                onClick={e => this.props.onPause(e)}
              >
                <i className="fas fa-pause" />
              </button>
            )}
            {this.props.whichPlay === "play" && (
              <button
                className="btn btn-play"
                onClick={e => this.props.onPlay(e)}
              >
                <i className="fas fa-play" />
              </button>
            )}

            <button
              className="btn btn-link"
              onClick={e => this.props.onNext(e)}
            >
              <i class="fas fa-forward" />
            </button>
          </div>
          <div>
            {/* {isFavorite === "kkboxList" && nowPlay && nowPlay.length !== 0 && (
              <span>{nowPlay && nowPlay[0].name}</span>
            )}
            {isFavorite === "followme" && nowPlay && nowPlay.length !== 0 && (
              <span>{nowPlay && nowPlay[0].snippet.title}</span>
            )} */}
            {nowPlay && nowPlay.length !== 0 && (
              <span>{nowPlay && nowPlay[0].snippet.title}</span>
            )}
          </div>
          {(isFavorite === "kkboxList" || isFavorite === "followme") && (
            <div className="ml-auto">
              <button
                className="btn btn-link btn-favorite"
                onClick={e => addToFavorite(e, nowPlay[0])}
              >
                <img
                  draggable="false"
                  className="emoji"
                  alt="💓"
                  src="https://twemoji.maxcdn.com/2/72x72/1f493.png"
                  style={{ width: "26px" }}
                />
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default YoutubePanel;
