import React from 'react'
import { loadScript } from '../../utils/config'
let ticking = false;
let lastScrollY = 0
class YouTubeIframe extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      nowPlayIdx: 0,
      nowPlayVideoID: '',
      nowPlay: this.props.nowPlay,
      isSticky: true
    }
    this.embedRef = React.createRef();
    loadScript('https://www.youtube.com/iframe_api')
    this.initYouTube()

  }
  // componentDidMount() {
  //   window.addEventListener('scroll', this.handleScroll);
  // }

  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.handleScroll);
  // }


//   handleScroll = () => {
//     lastScrollY = window.scrollY;
// // console.log(lastScrollY)
//     window.requestAnimationFrame(() => {
//       // this.embedRef.current.style.top = `${lastScrollY}px`;
//       if (lastScrollY > 130) {
//         this.setState({
//           isSticky: true
//         })
//       } else if (lastScrollY < 130) {
//         this.setState({
//           isSticky: false
//         })
//       }
//     });

//   };
  initYouTube = () => {
    window['onYouTubeIframeAPIReady'] = e => {
      this.YT = window['YT']
      this.reframed = false
      this.player = new window['YT'].Player('player', {
        videoId: this.props.id,
        playerVars: {
          autoplay: 1,
        },
        events: {
          onStateChange: this._onPlayerStateChange,
        },
      })
      //   this.player.playVideo()
    }
  }
  _onPause = () => this.player.pauseVideo()
  _onPlay = () => this.player.playVideo()
  cueVideoById = id => {
    this.player.cueVideoById({
      suggestedQuality: 'tiny',
      videoId: id,
    })
    this.player.playVideo()
    this.setState({
      nowPlayVideoID: id,
    })
  }
  _onPlayerStateChange = event => {
    if (event.data == this.YT.PlayerState.ENDED) {
      // 現在的清單，與現在播放的ID
      const nowPlayList = this.props.favoriteData
      const idx = nowPlayList
        .map((el, idx) => {
          return el.id
        })
        .indexOf(this.state.nowPlayVideoID)

      this.setState({
        nowPlayIdx: idx + 1,
      })
      // 如果撥到最後一首，跳回第一首
      if (nowPlayList.length === this.state.nowPlayIdx) {
        this.setState({
          nowPlayIdx: 0,
        })
      }
      if (event.data === 0) {
        this.cueVideoById(this.props.favoriteData[this.state.nowPlayIdx].id)
        this.player.playVideo()
        // 播放結束後，傳遞到父層，驅動數據
        this.props.getNowPlay(this.props.favoriteData[this.state.nowPlayIdx].id)
      }
      // your code to run after video has ended
      // player.current_video++;
      // playVideo()
    }
  }

  render() {
    return (
      <div class={`embed-responsive embed-responsive-16by9`} ref={this.embedRef}>
        <div id="player" className="embed-responsive-item"/>
      </div>
    )
  }
}

export default YouTubeIframe
