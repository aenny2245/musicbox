import React from "react";
import YouTubeProvider from "../../context/YouTubeProvider";
import Body from "./body";
export default function() {
  return (
    <YouTubeProvider>
      <Body />
    </YouTubeProvider>
  );
}
