import React from "react";
import { withRouter } from "react-router-dom";
import BookReviewData from "./bookreview.json";
import convert from "htmr";
import styled from "styled-components";

const h4Style = styled("h4")`
  margin-bottom: 1rem;
`;
const transform = {
  h4: h4Style
};

const BookReview = ({ match }) => {
  const bookData = BookReviewData && BookReviewData.results;
  const nowBook = bookData.filter((v, i) => v.book === match.params.id)[0];
  if (nowBook) {
    return (
      <div className="container">
        <div className="row mt-3">
          <div className="col-sm-3">
            <img src={nowBook.img} className="img-fluid" />
          </div>
          <div className="col-sm-9">
            <h4 className="mb-3">{nowBook.book_tw}</h4>
            {/* <div dangerouslySetInnerHTML={{__html:nowBook.review}}></div> */}
            {convert(nowBook.review, { transform })}
          </div>
        </div>
      </div>
    );
  } else {
    return 404;
  }
};
export default withRouter(BookReview);
