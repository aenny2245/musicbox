import React, { useState, useEffect } from "react";
import {Link} from 'react-router-dom'
import { dedupeByProperty } from "../../utils/config";

import "./styles.css";
import datas from "./res.json";
const RadioBox = props => {
  const change = e => {
    props.handleChange(e);
  };
  return (
    <div class="form-check form-check-inline">
      <input
        class="form-check-input"
        type="radio"
        name={props.name}
        id={props.id}
        value={props.value}
        onChange={e => change(e)}
        checked={props.checked}
      />
      <label class="form-check-label" for={props.id}>
        {props.value}
      </label>
    </div>
  );
};

const ZY135VIEW = () => {
  const areaList = [
    { name: "area", value: "all" },
    { name: "area", value: "台湾" },
    { name: "area", value: "韩国" },
    { name: "area", value: "美国" },
    { name: "area", value: "日本" },
    { name: "area", value: "法国" },
    { name: "area", value: "香港" },
    { name: "area", value: "大陆" },
    { name: "area", value: "泰国" },
    { name: "area", value: "西班牙" },
    { name: "area", value: "其它" }
  ];
  const ReleaseList = [
    { name: "Release", value: "all" },
    { name: "Release", value: "2019" },
    { name: "Release", value: "2018" },
    { name: "Release", value: "2017" },
    { name: "Release", value: "2016" },
    { name: "Release", value: "2015" },
    { name: "Release", value: "2014" }
  ];
  const typeList = [
    { name: "type", value: "all" },
    { name: "type", value: "剧情片" },
    { name: "type", value: "科幻片" },
    { name: "type", value: "动作片" },
    { name: "type", value: "爱情片" },
    { name: "type", value: "国产剧古装" },
    { name: "type", value: "动漫" },
    { name: "type", value: "日剧" },
    { name: "type", value: "韩剧" },
    { name: "type", value: "欧美剧" },
    { name: "type", value: "港剧" },
    { name: "type", value: "台剧" },
    { name: "type", value: "国产剧喜剧 科幻" },
    { name: "type", value: "国产剧" },
    { name: "type", value: "综艺" },
    { name: "type", value: "欧美剧喜剧" },
    { name: "type", value: "欧美剧犯罪" },
    { name: "type", value: "泰剧" },
    { name: "type", value: "欧美剧悬疑 犯罪" },
    { name: "type", value: "伦理片" },
    { name: "type", value: "欧美剧悬疑 犯罪" },
    { name: "type", value: "美女热舞写真" },
    { name: "type", value: "纪录片" },
    { name: "type", value: "VIP视频秀" },
    { name: "type", value: "街拍美女视频" }
  ];
  const [img, setImg] = useState([]);
  const [filterData, setfilterData] = useState([]);
  const [radioCheck, setRadioCheck] = useState({
    area: "all",
    Release: "all",
    Type: "all"
  });
  useEffect(() => {
    setImg(datas);
    setfilterData(datas);
  }, []);
  const handleChange = e => {
    // console.log(e.target.value);
    areaList.map(val => {
      val["checked"] = false;
      if (val.value === e.target.value) {
        val["checked"] = true;
      }
    });
    const checkedList = areaList.filter(val => {
      if (val.checked) {
        return val;
      }
    });
    setRadioCheck({ ...radioCheck, area: checkedList[0].value });
  };

  const hadleChangeRelease = e => {
    ReleaseList.map(val => {
      val["checked"] = false;
      if (val.value === e.target.value) {
        val["checked"] = true;
      }
    });
    const checkedRelease = ReleaseList.filter(val => {
      if (val.checked) {
        return val;
      }
    });
    setRadioCheck({ ...radioCheck, Release: checkedRelease[0].value });
  };
  const hadleChangeType = e => {
    typeList.map(val => {
      val["checked"] = false;
      if (val.value === e.target.value) {
        val["checked"] = true;
      }
    });
    const checkedType = typeList.filter(val => {
      if (val.checked) {
        return val;
      }
    });
    setRadioCheck({ ...radioCheck, Type: checkedType[0].value });
  };
  const acc = dedupeByProperty(img, "imgTitle");
  const aa = acc.filter(val => {
    // const sameDrama =
    const areaPipe =
      radioCheck.area === "all" ? true : val.infoArea[1] === radioCheck.area;
    const ReleasePipe =
      radioCheck.Release === "all"
        ? true
        : val.infoRelease[1] === radioCheck.Release;
    const TypePipe =
      radioCheck.Type === "all" ? true : val.infoType[1] === radioCheck.Type;
    if (areaPipe && ReleasePipe && TypePipe) {
      return val;
    }
  });
  if (filterData) {
    return (
      <div className="container">
        <div className="mb-2 w-100">{`目前筆數:${aa.length}`}</div>
        <div
          className="d-flex d-md-block flex-nowrap"
          style={{ overflowX: "auto" }}
        >
          {areaList.map((val, i) => {
            return (
              <RadioBox
                name={val.name}
                id={`${val.name}${i}`}
                value={val.value}
                handleChange={handleChange}
                checked={val.checked}
              />
            );
          })}
        </div>
        <div
          className="d-flex d-md-block  flex-nowrap"
          style={{ overflowX: "auto" }}
        >
          {ReleaseList.map((val, i) => {
            return (
              <RadioBox
                name={val.name}
                id={`${val.name}${i}`}
                value={val.value}
                handleChange={hadleChangeRelease}
                checked={val.checked}
              />
            );
          })}
        </div>
        <div
          className="d-flex d-md-block  flex-nowrap"
          style={{ overflowX: "auto" }}
        >
          {typeList.map((val, i) => {
            return (
              <RadioBox
                name={val.name}
                id={`${val.name}${i}`}
                value={val.value}
                handleChange={hadleChangeType}
                checked={val.checked}
              />
            );
          })}
        </div>

        <div className="row">
          {aa &&
            aa.map(v => {
              return (
                <div className="col-6 col-md-2">
                  <a className="cardInfo" href={v.imgUrl} target="_blank">
                    <img src={v.imgSrc} className="img-fluid" />
                    <h5>{v.imgTitle}</h5>
                    <ul className="listInfo">
                      <li>{`${v.infoArea[0]}${v.infoArea[1]}`}</li>
                      <li>{`${v.infoRelease[0]}${v.infoRelease[1]}`}</li>
                      <li>{`${v.infoType[0]}${v.infoType[1]}`}</li>
                      <li>{`${v.infoActor[0]}${v.infoActor[1]}`}</li>
                    </ul>
                  </a>
                </div>
              );
            })}
        </div>
        <Link to="zy135" className="goLink">
          Play
        </Link>
      </div>
    );
  } else {
    return <div>no data</div>;
  }
};

export default ZY135VIEW;
