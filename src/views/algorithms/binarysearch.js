import React from "react";
const binarysearch = () => {
  function Node(val) {
    this.value = val;
    this.left = null;
    this.right = null;
  }

  Node.prototype.search = function(val) {
    if (this.value == val) {
      console.log("found" + val);
      return this;
    } else if (val < this.value && this.left != null) {
      return this.left.search(val);
    } else if (val > this.value && this.right != null) {
      return this.right.search(val);
    }
    return null;
  };

  Node.prototype.visit = function() {
    if (this.left != null) {
      this.left.visit();
    }
    console.log(this.value);
    if (this.right != null) {
      this.right.visit();
    }
  };

  Node.prototype.addNode = function(n) {
    if (n.value < this.value) {
      if (this.left == null) {
        this.left = n;
      } else {
        this.left.addNode(n);
      }
    } else if (n.value > this.value) {
      if (this.right == null) {
        this.right = n;
      } else {
        this.right.addNode(n);
      }
    }
  };
  function Tree() {
    this.root = null;
  }

  Tree.prototype.traverse = function() {
    this.root.visit();
  };

  Tree.prototype.search = function(val) {
    var found = this.root.search(val);
    return found;
  };

  Tree.prototype.addNode = function(val) {
    console.log(val);
    var n = new Node(val);
    if (this.root === null) {
      this.root = n;
    } else {
      this.root.addNode(n);
    }
  };

  var tree;

  function setup() {
    // noCanvas();
    tree = new Tree();
    for (var i = 0; i < 10; i++) {
      tree.addNode(Math.floor(Math.random(0, 100)));
    }
    console.log(tree);
    tree.traverse();

    var result = tree.search(10);
    if (result == null) {
      console.log("not found");
    } else {
      console.log(result);
    }
  }
  setup();

  return (
    <div className="container">
      <div className="row">
        <div className="col-8 p-3">
          <p>什麼是二分演算法 ? </p>
          <p>隨機亂數 30 15 7 22 17 27 60 45 75 分別投入 node，</p>
          <p>
            大的放左邊，小的放右邊 第一個數為 30 因為沒有任何數字所以在第一格
          </p>
          <p>
            第二個數為 15 因為小於 30 所以放左邊 第三個數為 7 因為小於 30 小於
            15
          </p>
          <p>所以在最左邊 第四個數為 22 因為小於 30 大於 15 所以在最右邊</p>
          <p>如果沒有值，就回傳 null … 依此類推，就是二分演算法</p>
        </div>
        <div className="col-4 p-3">
          <img src={require("../../static/img/binary_search.png")} className="img-fluid"/>
        </div>
      </div>
    </div>
  );
};

export default binarysearch;
