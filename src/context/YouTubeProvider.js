import React, { Component } from "react";
import { withRouter } from "react-router-dom";

export const AppContext = React.createContext();

import { _getKKBOXFollow, _getYouTube, serialize } from "../utils/config";

class YouTubeProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "dashboard", //初始化
      favorites: ["BTC", "ETH", "XMR", "DOGE"],
      timeInterval: "months",
      ...this.savedSettings(), //立即函示(如果沒有 localStroage) ~
      setPage: this.setPage,
      addCoin: this.addCoin,
      removeCoin: this.removeCoin,
      isInFavorites: this.isInFavorites,
      confirmFavorites: this.confirmFavorites,
      setCurrentFavorite: this.setCurrentFavorite,
      setFilteredCoins: this.setFilteredCoins,
      changeChartSelect: this.changeChartSelect,
      kkboxFollowFn: this.kkboxFollowFn,
      kkboxFollowList: []
    };
  }

  // 載
  componentDidMount() {
    this.kkboxFollowData();
  }

  kkboxFollowData = async () => {
    let res = await _getKKBOXFollow();
    this.fetchPlaykkboxList(res.data.data)
  };

  fetchPlaykkboxList = async (data) => {
    const youtubeInfoList = await this.youtubeQueryInfo(data);
    this.setState({youtubeInfoList})
  };
  // 將 kkboxlist 的 api 資料，放進 Youtube api 搜尋 ，取得 youtube 音樂的 id
  youtubeQueryInfo = async (kkboxFollowList) => {
    const promises = await kkboxFollowList.reduce(
      async (acc, curr) => {
        const data = {
          q: `${curr.name} ${curr.artist.name}`,
          part: "snippet",
          maxResults: 1,
          // 排序
          order: "relevance" //date,viewCount,rating,relevance,videoCount
        };
        let asyncResult = await _getYouTube("search", serialize(data));
        if (asyncResult) {
          (await acc).push(asyncResult);
        }
        return acc;
      },
      []
    );
    return Promise.all(promises);
  };

  fetchCoins = async () => {
    let coinList = (await cc.coinList()).Data;
    this.setState({
      coinList: coinList
    });
  };

  fetchHistorical = async () => {
    if (this.state.firstVisit) return;
    let results = await this.historical();

    console.log(historical);
  };

  savedSettings() {
    let cryptoDashData = JSON.parse(localStorage.getItem("cryptoDash"));
    if (!cryptoDashData) {
      return { page: "settings", firstVisit: true };
    }
    let { favorites, currentFavorite } = cryptoDashData;
    return { favorites, currentFavorite };
  }

  setPage = page => {
    this.setState({ page });
  };

  render() {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    );
  }
}

export default YouTubeProvider;
