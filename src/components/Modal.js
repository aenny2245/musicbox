import React, { Component } from 'react'
import ReactDOM from 'react-dom'
const modalRoot = document.getElementById('modal-root')
export class Modal extends React.Component {
  constructor(props) {
    super(props)
    this.el = document.createElement('div')
    console.log(this.el)
  }
  componentDidMount() {
    // Append the element into the DOM on mount. We'll render
    // into the modal container element (see the HTML tab).
    modalRoot.appendChild(this.el)
  }

  componentWillUnmount() {
    // Remove the element from the DOM when we unmount
    modalRoot.removeChild(this.el)
  }
  render() {
    // 建立一個門戶，將門戶放進 this.el 內
    return ReactDOM.createPortal(this.props.children, this.el)
  }
}

export default Modal
