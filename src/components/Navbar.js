import React, { useState } from "react";
import ReactDOM from "react-dom";
import { NavLink } from "react-router-dom";
import pheonix from "../static/img/pheonix.svg";
import { connect } from "react-redux";
const Navbar = props => {
  const [form, setForm] = useState({ password: "" });
  console.log(props);
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <NavLink to="/" className="navbar-brand">
          <img src={pheonix} style={{ width: "30px" }} />
        </NavLink>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon" />
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <NavLink
                to="/youTube"
                className="nav-link"
                activeStyle={{
                  fontWeight: "bold",
                  color: "#ff5d43"
                }}
              >
                音樂盒子
              </NavLink>
            </li>

            {props.login && (
              <li class="nav-item">
                <NavLink
                  to="/zy135"
                  className="nav-link"
                  activeStyle={{
                    fontWeight: "bold",
                    color: "#ff5d43"
                  }}
                >
                  影片盒子
                </NavLink>
              </li>
            )}
            {props.login && (
              <li class="nav-item">
                <NavLink
                  to="/comic"
                  className="nav-link"
                  activeStyle={{
                    fontWeight: "bold",
                    color: "#ff5d43"
                  }}
                >
                  漫畫盒子
                </NavLink>
              </li>
            )}
            {props.login && (
              <li class="nav-item">
                <NavLink
                  to="/Food"
                  className="nav-link"
                  activeStyle={{
                    fontWeight: "bold",
                    color: "#ff5d43"
                  }}
                >
                  美食排行
                </NavLink>
              </li>
            )}
            {props.login && (
              <li class="nav-item">
                <NavLink
                  to="/Test"
                  className="nav-link"
                  activeStyle={{
                    fontWeight: "bold",
                    color: "#ff5d43"
                  }}
                >
                  測試區
                </NavLink>
              </li>
            )}
          </ul>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              {!props.login && (
                <React.Fragment>
                  <div className="d-flex flex-nowrap w-100">
                    <input
                      type="input"
                      value={form.password}
                      className="form-control mr-2"
                      onChange={e => {
                        setForm({ ...form, password: e.target.value });
                      }}
                    />
                    <button
                      className="btn btn-sm text-white btn-info nav-link px-3"
                      style={{ whiteSpace: "pre" }}
                      onClick={() => {
                        props.onLogin(form);
                      }}
                    >
                      登入
                    </button>
                  </div>
                </React.Fragment>
              )}
              {props.login && (
                <button
                  className="btn btn-sm text-white btn-info nav-link px-3"
                  onClick={() => {
                    setForm({ ...form, password: "" });
                    props.onLogout();
                  }}
                >
                  登出
                </button>
              )}
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

const mapStateToProps = state => {
  return {
    login: state.accountFn.login,
    password: state.accountFn.password
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: values => dispatch({ type: "LOGIN", account: values }),
    onLogout: () => dispatch({ type: "LOGOUT" })
  };
};

const connectNavbar = connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar);

export default connectNavbar;
