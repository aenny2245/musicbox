import React, { Component } from 'react'

export class Sidebar extends Component {
  constructor(props) {
    super(props)
    this.sidebar_nav = React.createRef()
  }
  navToggle = e => {
    this.sidebar_nav.classList.toggle('active')
  }
  render() {
    return (
      <div className="sidebar sticky-top">
        {/* <button className="btn btn-navToggle " onClick={this.navToggle}>
          hamb
        </button> */}
        <nav className="sidebar_nav" ref={el => (this.sidebar_nav = el)}>
          <h6 className="sidebar_title">分類</h6>
          <a href="javascript:;" className="sidebar_nav-item">
            <i class="fas fa-music" />
            音樂
          </a>
          <a href="javascript:;" className="sidebar_nav-item">
            <i class="fas fa-torah" />
            專輯
          </a>
          <a href="javascript:;" className="sidebar_nav-item">
            <i class="fas fa-users" />
            歌手
          </a>
          <a href="javascript:;" className="sidebar_nav-item">
            <i class="fas fa-heart" />
            我的最愛
          </a>
          <a href="javascript:;" className="sidebar_nav-item" onClick={this.props.searchModel}>
            <i class="fas fa-heart" />
            搜尋
          </a>
        </nav>
        {this.props.children}
      </div>
    )
  }
}

export default Sidebar
