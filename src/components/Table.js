import React, { useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
const Table = ({
  column,
  datas,
  summary,
  pagination,
  paginationInit,
  className,
  theme,
  currentPageLink,
  history
}) => {
  const [perPage, setPerPage] = useState(paginationInit); //一頁幾筆資料
  const [currentPage, setCurrentPage] = useState(1); // 目前在第幾頁
  const [paginationNums, setPaginationNums] = useState([]); // 總共有幾個頁面
  useEffect(() => {
    setCurrentPage(currentPageLink);
  }, [currentPageLink]);
  useEffect(() => {
    const arr = [];
    for (let i = 1; i <= Math.ceil(datas.length / perPage); i++) {
      arr.push(i);
    }
    setPaginationNums(arr);
  });
  const handleChange = e => {
    setPerPage(e.target.value);

    setCurrentPage(1);
  };
  return (
    <React.Fragment>
      <div className="table-feature d-flex mb-2">
        <div className="left mr-auto">
          每頁顯示筆數
          <select className="select-yellow" onChange={handleChange}>
            {pagination.map((v, i) => {
              return <option value={v}>{v}</option>;
            })}
          </select>
        </div>

        <div class="right table-pagination">
          <ul class="pagination">
            <li class="page-item">
              <a
                className={`page-link`}
                onClick={e => {
                  setCurrentPage(currentPage > 1 ? currentPage - 1 : 1);
                  if (+currentPageLink > 1) {
                    history.push(
                      `/components/tablepage/${+currentPageLink - 1}`
                    );
                  }
                }}
              >
                上一頁
              </a>
            </li>
            {paginationNums.map((v, i) => {
              return (
                <li class="page-item">
                  <Link
                    to={`/components/tablepage/${v}`}
                    className={`${
                      v === +currentPageLink
                        ? `page-link btn-${theme}`
                        : `page-link`
                    }`}
                  >
                    {v}
                  </Link>
                </li>
              );
            })}

            <li class="page-item">
              <a
                className={`page-link`}
                onClick={e => {
                  setCurrentPage(
                    currentPageLink < paginationNums.length
                      ? currentPageLink + 1
                      : paginationNums.length
                  );
                  if (+currentPageLink < paginationNums.length) {
                    history.push(
                      `/components/tablepage/${+currentPageLink + 1}`
                    );
                  }
                }}
              >
                下一頁
              </a>
            </li>
          </ul>
        </div>
      </div>
      <table summary={summary} className={className}>
        <thead className={`bg-${theme} text-white`}>
          <tr>
            {column.map((col, idx) => {
              return <th>{col.name}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {datas.length === 0 ? (
            <tr>
              <td colSpan={column.length}>沒有資料</td>
            </tr>
          ) : (
            datas
              .slice(perPage * (currentPage - 1), currentPage * perPage)
              .map((data, idx, arr) => {
                return (
                  <tr key={data._id}>
                    {column.map((col, i) => {
                      if (col.hasOwnProperty("render")) {
                        return (
                          <td>
                            {col.render(
                              data,
                              idx + perPage * (currentPage - 1),
                              arr
                            )}
                          </td>
                        );
                      }
                      return <td>{data[col.prop]}</td>;
                    })}
                  </tr>
                );
              })
          )}
        </tbody>
      </table>
    </React.Fragment>
  );
};

export default withRouter(Table);
