import React, { useRef, useState, useEffect } from 'react'
import { useGoogleMap, useMap } from '../hooks/useMap.js'
const API_KEY = 'AIzaSyCmeCDLj1uvr97KxjUiaade1zfeya02Uog'

const initialConfig = {
  zoom: 15,
  center: { lat: 35.6432027, lng: 139.6729435 }
}
// hookを利用して表示するコンポーネント

const Gmap = ({ address, latLng }) => {
  let [propAdress, setPropAdress] = useState(null)
  const googleMap = useGoogleMap(API_KEY)
  const mapContainerRef = useRef(null)

  useMap({ googleMap, mapContainerRef, initialConfig, latLng })

  if (address !== propAdress) {
    if (address !== undefined) {
      console.log(address['地址'])
    }
    // Row changed since last render. Update isScrollingDown.
  }
  return (
    <div
      style={{
        height: '100vh',
        width: '100%'
      }}
      ref={mapContainerRef}
    />
  )
}

export default Gmap
