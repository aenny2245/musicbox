import React, { Component } from 'react'
import { Router, Route } from 'react-router-dom'
import YouTubeAPI from './pages/YouTubeAPI/index'

export class router extends Component {
  render() {
    return <Route exact path="/" component={YouTubeAPI} />
  }
}
export default router
